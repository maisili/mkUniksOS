{ kor, hyraizyn, deriveicynz, redjistri }:
let
  inherit (kor) optionals mkIf optional;

  inherit (hyraizyn.astra.io) disks swapDevices butlodyr;

  inherit (hyraizyn.methydz.astra) izBildyr izDispatcyr izNiksKac
  neim nbOfBildKorz yggdrasil;

  inherit (hyraizyn.methydz.metastra) bildyrKonfigz kacURLz priKriomz;

  niksPriKriokorPath = "/etc/nix/priKriokor";

  nixServeInternalPort = 4999;
  nixServeExternalPort = 5000;

in {
  boot = {
    kernelParams = [ "consoleblank=300" ];

    loader = {
      grub.enable = butlodyr == "mbr";
      systemd-boot.enable = butlodyr == "uefi";
      efi.canTouchEfiVariables = butlodyr == "uefi";
      generic-extlinux-compatible.enable = butlodyr == "uboot";
    };

  };

  fileSystems = disks;
  inherit swapDevices;

  networking = {
    hostName = neim;
    dhcpcd.extraConfig = "noipv4ll";
  };

  nix = {
    package = deriveicynz.niks;

    trustedUsers = [ "root" "@nixdev" ];

    allowedUsers = [ "@users" "nix-serve" ]
    ++ optional izBildyr  "niksBildyr";

    buildCores = nbOfBildKorz;
    daemonNiceLevel = 19;
    daemonIONiceLevel = 7;

    sandboxPaths = [ "/niks"  "/uniks" ];

    binaryCachePublicKeys = priKriomz.niksBildyrz;
    binaryCaches = kacURLz;
    trustedBinaryCaches = kacURLz;

    autoOptimiseStore = true;

    extraOptions = ''
        flake-registry = ${redjistri}
        experimental-features = nix-command flakes ca-references recursive-nix
        secret-key-files = ${niksPriKriokorPath}
    '';

    sshServe = {
      enable = izNiksKac;
      keys = priKriomz.eseseitc;
      protocol = "ssh";
    };

    distributedBuilds = izDispatcyr;
    buildMachines = optionals izDispatcyr bildyrKonfigz;

  };

  users = {
    groups.nixdev = {};
    defaultUserShell = deriveicynz.mksh;

    users = mkIf izBildyr {
      niksBildyr = {
        useDefaultShell = true;
        openssh.authorizedKeys.keys = priKriomz.dispatcyrz;
      };
    };

  };

  services = {
    nix-serve = {
      enable = izNiksKac;
      bindAddress = "127.0.0.1";
      port = nixServeInternalPort;
      secretKeyFile = niksPriKriokorPath;
    };

    nginx = {
      enable = izNiksKac;
      virtualHosts."[${yggdrasil.address}]:${toString nixServeExternalPort}" = {
        listen = [ { addr = "[${yggdrasil.address}]"; port = nixServeExternalPort; } ];
        locations."/".proxyPass = "http://127.0.0.1:${toString nixServeInternalPort}";
      };
    };

  };

}
