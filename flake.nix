{
  description = "mkUniksOS";

  outputs = {
    self, kor,
    gluNixOS, mkPkgs, mkDeriveicynz, hyraizyn, redjistri
  }@fleiks:
  {
    datom = hyraizyn:
    let
      kor = fleiks.kor.datom;

      redjistri = fleiks.redjistri.datom;

      localSystem = { system = fleiks.hyraizyn.datom.methydz.astra.sistym; };
      crossSystem = { system = hyraizyn.methydz.astra.sistym; };

      pkgs = mkPkgs.datom {
        inherit localSystem crossSystem;
      };

      deriveicynz = mkDeriveicynz.datom {
        inherit hyraizyn pkgs;
      };

      mkBeisNixosConfig = import ./mkBeisNixosConfig.nix;

      beisNixosConfig = mkBeisNixosConfig {
        inherit kor hyraizyn deriveicynz redjistri;
      };

      nixpkgsConfig = {
        nixpkgs = {
          inherit pkgs localSystem crossSystem;
        };
      };

      nixosConfigz = [
        beisNixosConfig
        nixpkgsConfig
      ];

      evalUniksOS = gluNixOS.datom.evalNixOS {
        moduleArgs = {
          inherit hyraizyn pkgs kor;
        };
        modules = nixosConfigz;
      };

      bildUniksOS = evalUniksOS.config.system.build.toplevel;

    in bildUniksOS;

  };
}
